# Elastic search training

## General 
 
This install elatic search version 5.x on ubuntu 16.04. Ansible provisioning base on [Daniel Berman playbook]: https://github.com/DanielBerman/ansible-elk-playbook/blob/master/site.yml

## Requirements

### Hardware

* At least 4 GB available RAM - if is not possible please do not panic we may adjust vagrant machine
* CPU wit enabled in BIOS vitalization - that is must have! 

### Software

* [ Vagrant ](http://docs.vagrantup.com/v2/installation/)
* [ VirtualBox ](https://www.virtualbox.org/wiki/Downloads)
* [ Postman app ](https://www.getpostman.com/downloads/)

Training machine has been tested with setup: Vagrant 2.2.4, Ansible 2.7.8, Virtuialbox 5.2.18

## Installation

* Clone repo.

* Create elastic machine with Vagrant
```sh
vagrant up
```

If during first run some error has been reported please perform
```sh
vagrant reload
```
If your machine has been created without error, you can start playing with elastic search

```sh
http://localhost:9200/_cluster/health
``` 

To create second elastic node you have to explicitly tell vagrant to do that

```sh
vagrant up node2
```

If everything will be OK you should be able to see two nodes

```sh
http://localhost:9200/_cluster/state/nodes
```
